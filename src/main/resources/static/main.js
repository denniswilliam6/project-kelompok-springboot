/**
 * 
 */

$('document').ready(function() {
	
	$('.table .btn-warning').on('click',function(event){
		
		event.preventDefault();
		
		var href= $(this).attr('href');
		
		$.get(href, function(order, status){
			$('#IdEdit').val(order.id);
			$('#nameEdit').val(order.tanggalPembelian);
			$('#departmentEdit').val(order.totalBayar);
			$('#updatedByEdit').val(order.user.nama);
			$('#updatedByEdit').val(order.status);
		});	
		
		$('#editModal').modal();
		
	});
	
});