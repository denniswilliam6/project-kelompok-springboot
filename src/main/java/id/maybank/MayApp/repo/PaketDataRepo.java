package id.maybank.MayApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.entity.PaketData;

public interface PaketDataRepo extends JpaRepository<PaketData, Long>{

}
