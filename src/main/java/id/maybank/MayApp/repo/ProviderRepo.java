package id.maybank.MayApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.entity.Provider;

public interface ProviderRepo extends JpaRepository <Provider, Long>{

}
