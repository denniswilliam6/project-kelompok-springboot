package id.maybank.MayApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.entity.BeliPaket;

public interface BeliPaketRepo extends JpaRepository<BeliPaket, Long>{

}
