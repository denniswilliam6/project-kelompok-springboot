package id.maybank.MayApp.Internet;


import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
public class PembelianInternet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String provider;

    private String nomor;

    private String paket;

    private String statusNet;

    private String metodeBayar;

    @ManyToOne
    @JoinColumn(name = "id_internet", referencedColumnName = "id")
    private Internet internet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getPaket() {
        return paket;
    }

    public void setPaket(String paket) {
        this.paket = paket;
    }

    public String getStatusNet() {
        return statusNet;
    }

    public void setStatusNet(String statusNet) {
        this.statusNet = statusNet;
    }

    public String getMetodeBayar() {
        return metodeBayar;
    }

    public void setMetodeBayar(String metodeBayar) {
        this.metodeBayar = metodeBayar;
    }

    public Internet getInternet() {
        return internet;
    }

    public void setInternet(Internet internet) {
        this.internet = internet;
    }
}
