package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InternetRepo extends JpaRepository<Internet, Long> {

    Internet findInternetById(Long id);
}
