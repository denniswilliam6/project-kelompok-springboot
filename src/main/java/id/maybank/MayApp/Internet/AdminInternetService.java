package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminInternetService {

    public List<AdminInternet> getAllAdminInternet();

    public void save(AdminInternet adminInternet);

    public void deleteData(Long id);


}
