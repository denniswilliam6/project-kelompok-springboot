package id.maybank.MayApp.Internet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminInternetRepo extends JpaRepository<AdminInternet, Long> {
}
