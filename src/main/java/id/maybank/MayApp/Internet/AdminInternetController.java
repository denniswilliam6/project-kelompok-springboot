package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admininternet")
public class AdminInternetController {

    @Autowired
    AdminInternetService adminInternetService;

    @Autowired
    InternetService internetService;

    @GetMapping
    public String index(Model model){
        List<Internet> internets = this.internetService.getAllInternet();
        model.addAttribute("admininternets", internets);
        model.addAttribute("internetForm", new Internet());
        return "admininternet";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("internetForm") Internet internet,
                       @ModelAttribute("internets") Internet internets,
                       Model model, BindingResult result){
        this.internetService.save(internet);
        return "redirect:/admininternet";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam("id") Long id){
        this.internetService.delete(id);
        return "redirect:/admininternet";
    }

}
