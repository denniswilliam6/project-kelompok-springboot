package id.maybank.MayApp.Internet;

import javax.persistence.*;

@Entity
@Table(name = "admininternet")
public class AdminInternet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String kecepatan;

    private String harga;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKecepatan() {
        return kecepatan;
    }

    public void setKecepatan(String kecepatan) {
        this.kecepatan = kecepatan;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
