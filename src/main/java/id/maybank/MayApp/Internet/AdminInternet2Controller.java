package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/admininternet2")
public class AdminInternet2Controller {


    @Autowired
    InternetService internetService;

    @Autowired
    PembelianService pembelianService;

    @GetMapping
    public String index(Model model){

        List<PembelianInternet> pembelianInternets = this.pembelianService.getAllPembelian();
        List<Internet> internets = this.internetService.getAllInternet();
        model.addAttribute("pembelianForm", new PembelianInternet());
        model.addAttribute("internet",pembelianInternets);
        model.addAttribute("internets",internets);
        return "admininternet2";
    }

    @GetMapping("/save")
    public String save(@ModelAttribute("pembelianinternets") PembelianInternet pembelianInternet,
                            @RequestParam("id")Long id, Model model, RedirectAttributes redirectAttributes){

        PembelianInternet pembelianInternet1= this.pembelianService.getByPembelianId(id);

        pembelianInternet1.setStatusNet("Anda telah berlangganan");
        this.pembelianService.save(pembelianInternet1);
        redirectAttributes.addFlashAttribute("success","data inserted");
        return "redirect:/admininternet2";
    }
}
