package id.maybank.MayApp.Internet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AdminInternetServiceImpl implements AdminInternetService{

    @Autowired
    private AdminInternetRepo adminInternetRepo;

    @Override
    public List<AdminInternet> getAllAdminInternet(){
        return this.adminInternetRepo.findAll();
    }

    @Override
    public void save(AdminInternet adminInternet){
        this.adminInternetRepo.save(adminInternet);
    }

    @Override
    public void deleteData(Long id){
        this.adminInternetRepo.deleteById(id);
    }
}
