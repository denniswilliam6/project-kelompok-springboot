package id.maybank.MayApp.Internet;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface InternetService {

    public void save(Internet internet);

    public List<Internet> getAllInternet();

    public Internet getInternetById(Long Id);

    public  void delete(Long id);
}
