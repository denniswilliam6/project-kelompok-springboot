package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class InternetServiceImpl implements InternetService{

    @Autowired
    private InternetRepo internetRepo;

    @Override
    public void save(Internet internet){
        this.internetRepo.save(internet);
    }

    @Override
    public List<Internet>getAllInternet(){
        return this.internetRepo.findAll();
    }

    @Override
    public Internet getInternetById(Long Id){
        return this.internetRepo.findInternetById(Id);
    }

    @Override
    public void delete(Long id) {
        this.internetRepo.deleteById(id);
    }


}
