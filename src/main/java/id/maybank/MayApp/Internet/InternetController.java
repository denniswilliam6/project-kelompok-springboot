package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.AdminListrik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/internet")
public class InternetController {

    @Autowired
    private InternetService internetService;

    @Autowired
    private PembelianService pembelianService;

    @GetMapping
    public String index(Model model){
        List<PembelianInternet> pembelianInternets = this.pembelianService.getAllPembelian();
        List<Internet> internets = this.internetService.getAllInternet();
        model.addAttribute("pembelianForm", new PembelianInternet());
        model.addAttribute("internet",pembelianInternets);
        model.addAttribute("internets",internets);

        List<String> providerList = Arrays.asList("Indihome", "Biznet", "My Republik");
        model.addAttribute("providerList", providerList);

        List<String> pembayaranList = Arrays.asList("Maybank", "Mandiri", "GOPAY", "BCA", "BRI");
        model.addAttribute("pembayaranList", pembayaranList);

        return "internet";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("internet") Internet internet,
                       @ModelAttribute("pembelianForm") PembelianInternet pembelianInternet,
                       /*@ModelAttribute("adminInternetForm") AdminInternet adminInternet,*/
                       Model model, BindingResult result){
        if (result.hasErrors()){
            return "listrik";
        }
        pembelianInternet.setInternet(internet);
        pembelianInternet.setPaket(internet.getKecepatan());
        pembelianInternet.setStatusNet("Menunggu Pembayaran");
        this.pembelianService.save(pembelianInternet);
        /*this.adminInternetService.save(adminInternet);*/
        return "redirect:/internet";
    }
}
