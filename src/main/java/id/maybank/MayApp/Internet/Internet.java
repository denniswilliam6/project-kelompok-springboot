package id.maybank.MayApp.Internet;

import javax.persistence.*;

@Entity
@Table(name = "internet")
public class Internet {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        private String kecepatan;

        private String harga;

        private String status;

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getKecepatan() {
                return kecepatan;
        }

        public void setKecepatan(String kecepatan) {
                this.kecepatan = kecepatan;
        }

        public String getHarga() {
                return harga;
        }

        public void setHarga(String harga) {
                this.harga = harga;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }
}
