package id.maybank.MayApp.Internet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PembelianRepo extends JpaRepository<PembelianInternet, Long> {
    PembelianInternet findPembelianById(Long Id);
}
