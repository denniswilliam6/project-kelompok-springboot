package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;

import java.util.List;

public interface PembelianService {

    public void save(PembelianInternet pembelianInternet);

    public List<PembelianInternet> getAllPembelian();

    public PembelianInternet getByPembelianId(Long Id);

}
