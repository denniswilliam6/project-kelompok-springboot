package id.maybank.MayApp.Internet;

import id.maybank.MayApp.ListrikNet.Listrik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PembelianServiceImpl implements PembelianService{

    @Autowired
    public PembelianRepo pembelianRepo;


    public void save(PembelianInternet pembelianInternet){
        this.pembelianRepo.save(pembelianInternet);

    }

    public List<PembelianInternet> getAllPembelian(){
        return this.pembelianRepo.findAll();

    }

    @Override
    public PembelianInternet getByPembelianId(Long Id) {
        return this.pembelianRepo.findPembelianById(Id);
    }
}
