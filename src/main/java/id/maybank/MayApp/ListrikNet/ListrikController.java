package id.maybank.MayApp.ListrikNet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/listrik")
public class ListrikController {

    @Autowired
    private ListrikService listrikService;

    @Autowired
    private AdminListrikService adminListrikService;

    @GetMapping
    public String index(Model model){
        List<Listrik> listriks = this.listrikService.getAllListrik();
        List<AdminListrik> adminListriks = this.adminListrikService.getAllAdminListrik();
        model.addAttribute("listrik", listriks);
        model.addAttribute("listrikForm", new Listrik());

        //*list pembayaran*//

        List<String> pembayaranList = Arrays.asList("Maybank", "Mandiri", "GOPAY", "BCA", "BRI");
        model.addAttribute("pembayaranList", pembayaranList);
        return "listrik";
    }
    @PostMapping("/save")
    public String save(@ModelAttribute("listrikForm") Listrik listrik,
                       Model model, BindingResult result){
        if (result.hasErrors()){
            return "listrik";
        }
        listrik.setStatus("Menunggu Pembayaran");
        this.listrikService.save(listrik);
        return "redirect:/listrik";

    }
}
