package id.maybank.MayApp.ListrikNet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AdminListrikServiceImpl implements AdminListrikService{

    @Autowired
    private AdminListrikRepo adminListrikRepo;

    @Override
    public void save(AdminListrik adminListrik){
        this.adminListrikRepo.save(adminListrik);
    }

    @Override
    public void deleteData(Long id){
        this.adminListrikRepo.deleteById(id);
    }

    public List<AdminListrik> getAllAdminListrik(){
        return this.adminListrikRepo.findAll();
    }

    @Override
    public Optional<AdminListrik> getAdminListrikById(Long id) {
        return this.adminListrikRepo.findById(id);

    }
}
