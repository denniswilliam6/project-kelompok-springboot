package id.maybank.MayApp.ListrikNet;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "listrik")
public class Listrik {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String noMeteran;

    private double nominal;

    private String status;

    private String metodeBayar;

    private Integer token;

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public String getMetodeBayar() {
        return metodeBayar;
    }

    public void setMetodeBayar(String metodeBayar) {
        this.metodeBayar = metodeBayar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoMeteran() {
        return noMeteran;
    }

    public void setNoMeteran(String noMeteran) {
        this.noMeteran = noMeteran;
    }

    public double getNominal() {
        return nominal;
    }

    public void setNominal(double nominal) {
        this.nominal = nominal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
