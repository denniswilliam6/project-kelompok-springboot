package id.maybank.MayApp.ListrikNet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ListrikRepo extends JpaRepository<Listrik, Long> {

    Listrik findByNoMeteran(String noMeteran);

    Listrik findByNominal(Double nominal);

    Listrik findByStatus(String status);

    Listrik findListrikById(Long id);
}
