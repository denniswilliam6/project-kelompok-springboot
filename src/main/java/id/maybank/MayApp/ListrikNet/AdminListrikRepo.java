package id.maybank.MayApp.ListrikNet;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminListrikRepo extends JpaRepository<AdminListrik, Long> {
}
