package id.maybank.MayApp.ListrikNet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ListrikServiceImpl implements ListrikService {



    @Autowired
    private ListrikRepo listrikRepo;

    @Override
    public void save(Listrik listrik){
        this.listrikRepo.save(listrik);
    }

    @Override
    public List<Listrik>getAllListrik(){
        return this.listrikRepo.findAll();
    }

    @Override
    public Listrik getByListrikId(Long Id) {
        return this.listrikRepo.findListrikById(Id);
    }

    @Override
    public Listrik findByNoMeteran(String noMeteran){
        return this.listrikRepo.findByNoMeteran(noMeteran);
    }

    @Override
    public Listrik findByNominal(Double nominal){
        return this.listrikRepo.findByNominal(nominal);
        }

        public Listrik findByStatus(String status){
        return this.listrikRepo.findByStatus(status);
        }


    }





