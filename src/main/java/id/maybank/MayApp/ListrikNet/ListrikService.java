package id.maybank.MayApp.ListrikNet;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ListrikService {

    public void save(Listrik listrik);

    public List<Listrik> getAllListrik();

    public Listrik getByListrikId(Long Id);

    public Listrik findByNoMeteran(String noMeteran);

    public Listrik findByNominal(Double nominal);

    public Listrik findByStatus(String status);

}
