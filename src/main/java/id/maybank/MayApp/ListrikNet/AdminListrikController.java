package id.maybank.MayApp.ListrikNet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/adminlistrik")
public class AdminListrikController {

    @GetMapping
    public String index(Model model){
        /*get data nomor meteran dan nominal pembelian*/
        List<Listrik> listriks = this.listrikService.getAllListrik();
        model.addAttribute("listriks",listriks);
        model.addAttribute("listrikForm", new Listrik());
        return "adminlistrik";
    }

    @Autowired
    private ListrikService listrikService;

    @Autowired
    private AdminListrikService adminListrikService;

    @GetMapping("/save")
    public String saveBayar(@ModelAttribute("listriks") Listrik listrik,
                        /*@ModelAttribute("adminListrikForm") AdminListrik adminListrik,*/
                        @RequestParam("id")Long id, Model model, RedirectAttributes redirectAttributes){

        /*this.adminListrikService.save(adminListrik);*/
        Listrik listrik1 = this.listrikService.getByListrikId(id);

        Double randomInteger = Math.random() * 10000000.0;

        listrik1.setStatus("Pembayaran diterima");
        listrik1.setToken((int) Math.abs(randomInteger));
        this.listrikService.save(listrik1);
        redirectAttributes.addFlashAttribute("success","data inserted");
        return "redirect:/adminlistrik";
    }





}

