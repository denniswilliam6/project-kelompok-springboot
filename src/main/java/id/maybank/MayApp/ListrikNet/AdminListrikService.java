package id.maybank.MayApp.ListrikNet;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AdminListrikService {

    public void save(AdminListrik adminListrik);

    public void deleteData(Long id);

    public List<AdminListrik> getAllAdminListrik();

    public Optional<AdminListrik> getAdminListrikById(Long id);


}
