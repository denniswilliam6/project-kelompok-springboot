package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penumpang;
import id.maybank.MayApp.pesawat.repo.JadwalRepo;

@Service
public class JadwalServiceImpl implements JadwalService {
	
	@Autowired
	private JadwalRepo jadwalRepo;
	
	@Autowired
	private PenumpangService penumpangService;

	@Override
	public List<Jadwal> getAll() {
		// TODO Auto-generated method stub
		return this.jadwalRepo.findAll();
	}

	@Override
	public void save(Jadwal jadwal) {
		
		this.jadwalRepo.save(jadwal);

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.jadwalRepo.deleteById(id);
		
	}


	@Override
	public Optional<Jadwal> getJadwalById(Long id) {
		// TODO Auto-generated method stub
		return this.jadwalRepo.findById(id);
	}

	@Override
	public Page<Jadwal> getAllPaginate(int pageNo, int pageSize, String sortField) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		
		return this.jadwalRepo.findAll(paging);	
		}

	@Override
	public Page<Jadwal> getAllPaginateSearch(int pageNo, int pageSize, String sortField, String search) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.jadwalRepo.search(search, paging);	
		}

	@Override
	public Jadwal getJadwalById2(Long id) {
		// TODO Auto-generated method stub
		return this.jadwalRepo.findJadwalById(id);
	}


	@Override
	public Jadwal getJadwalByFlight(String flight) {
		// TODO Auto-generated method stub
		return this.jadwalRepo.findJadwalByFlight(flight);
	}

	@Override
	public List<Jadwal> getAllSearch(String search, String search2) {
		// TODO Auto-generated method stub
		return this.jadwalRepo.search2(search,search2);
	}














}
