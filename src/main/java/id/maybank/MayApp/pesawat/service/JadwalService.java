package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import id.maybank.MayApp.pesawat.entity.Jadwal;

public interface JadwalService {
	
	public List<Jadwal> getAll();
	public void save(Jadwal jadwal);
	public void delete(Long id);
//	public Provider getBankByNama(String nama);
	public Page<Jadwal> getAllPaginate(int pageNo, int pageSize, String sortField);
	public Page<Jadwal> getAllPaginateSearch(int pageNo, int pageSize, String sortField, String search);
	public Optional<Jadwal> getJadwalById(Long id);
	public Jadwal getJadwalById2(Long id);
	public Jadwal getJadwalByFlight(String flight);
	public List<Jadwal> getAllSearch(String search, String search2);

}
