package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penumpang;

public interface PenumpangService {
	
	public List<Penumpang> getAll();
	public void save(Penumpang penumpang);
	public void delete(Long id);
//	public Provider getBankByNama(String nama);
	public Page<Penumpang> getAllPaginate(int pageNo, int pageSize, String sortField);
//	public Page<Penumpang> getAllPaginateSearch(int pageNo, int pageSize, String sortField, String search);
	public Optional<Penumpang> getPenumpangById(Long id);

}
