package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penumpang;
import id.maybank.MayApp.pesawat.repo.JadwalRepo;
import id.maybank.MayApp.pesawat.repo.PenumpangRepo;

@Service
public class PenumpangServiceImpl implements PenumpangService {
	
	@Autowired
	private PenumpangRepo penumpangRepo;

	@Override
	public List<Penumpang> getAll() {
		// TODO Auto-generated method stub
		return this.penumpangRepo.findAll();
	}

	@Override
	public void save(Penumpang penumpang) {
		// TODO Auto-generated method stub
		this.penumpangRepo.save(penumpang);

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.penumpangRepo.deleteById(id);
		
	}


	@Override
	public Optional<Penumpang> getPenumpangById(Long id) {
		// TODO Auto-generated method stub
		return this.penumpangRepo.findById(id);
	}

	@Override
	public Page<Penumpang> getAllPaginate(int pageNo, int pageSize, String sortField) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		
		return this.penumpangRepo.findAll(paging);	
		}

//	@Override
//	public Page<Penumpang> getAllPaginateSearch(int pageNo, int pageSize, String sortField, String search) {
//		// TODO Auto-generated method stub
//		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
//		return this.penumpangRepo.search(search, paging);	
//		}
//




}
