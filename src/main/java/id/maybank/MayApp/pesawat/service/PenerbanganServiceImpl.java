package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penerbangan;
import id.maybank.MayApp.pesawat.repo.JadwalRepo;
import id.maybank.MayApp.pesawat.repo.PenerbanganRepo;

@Service
public class PenerbanganServiceImpl implements PenerbanganService {
	
	@Autowired
	private PenerbanganRepo penerbanganRepo;
	
	@Autowired
	private JadwalRepo jadwalRepo;
	
	@Autowired
	private JadwalService jadwalService;

	@Override
	public void save(Penerbangan penerbangan) {
		// TODO Auto-generated method stub
		this.penerbanganRepo.save(penerbangan);

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.penerbanganRepo.deleteById(id);
	}


	@Override
	public Page<Jadwal> getAllPaginate(int pageNo, int pageSize, String sortField) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		
		return this.jadwalRepo.findAll(paging);		}

	@Override
	public Optional<Penerbangan> getPenerbanganById(Long id) {
		// TODO Auto-generated method stub
		return this.penerbanganRepo.findById(id);
	}

	@Override
	public List<Penerbangan> getAll() {
		// TODO Auto-generated method stub
		return this.penerbanganRepo.findAll();
	}

	@Override
	public Page<Penerbangan> getAllPaginates(int pageNo, int pageSize, String sortField) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		
		return this.penerbanganRepo.findAll(paging);		}



	

}
