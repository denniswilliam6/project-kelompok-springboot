package id.maybank.MayApp.pesawat.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penerbangan;

public interface PenerbanganService {
	
	public List<Penerbangan> getAll();
	public void save(Penerbangan penerbangan);
	public void delete(Long id);
//	public Provider getBankByNama(String nama);
	public Page<Jadwal> getAllPaginate(int pageNo, int pageSize, String sortField);
	public Page<Penerbangan> getAllPaginates(int pageNo, int pageSize, String sortField);

	public Optional<Penerbangan> getPenerbanganById(Long id);

}
