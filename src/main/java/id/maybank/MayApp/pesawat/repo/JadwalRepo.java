package id.maybank.MayApp.pesawat.repo;


import java.util.List;

import org.hibernate.validator.constraints.ParameterScriptAssert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.maybank.MayApp.pesawat.entity.Jadwal;

public interface JadwalRepo extends JpaRepository<Jadwal, Long> {
	
	@Query(value = "SELECT * FROM jadwal s WHERE"
			+ " s.flight iLIKE %:search%"
			+ " OR s.source iLIKE %:search%"
			+ " OR s.destination iLIKE %:search%", nativeQuery = true)
	Page<Jadwal> search( @Param("search") String search, PageRequest paging);
	

	
	@Query(value = "SELECT * FROM jadwal s WHERE"
			+ " s.source iLIKE %:search%"
			+ " AND s.destination iLIKE %:search2%", nativeQuery = true)
	List<Jadwal> search2(@Param("search") String search, @Param("search2") String search2);
	 	
	Jadwal findJadwalById(Long id);
	
	Jadwal findJadwalByFlight(String flight);

}
