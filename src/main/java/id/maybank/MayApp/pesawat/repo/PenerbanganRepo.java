package id.maybank.MayApp.pesawat.repo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.maybank.MayApp.pesawat.entity.Penerbangan;

public interface PenerbanganRepo extends JpaRepository<Penerbangan, Long> {

	@Query(value = "SELECT * FROM jadwal s WHERE s.source iLIKE %:search%"
			+ " OR s.destination iLIKE %:search1%", nativeQuery = true)
	Page<Penerbangan> search( @Param("search") String search, PageRequest paging);


}
