package id.maybank.MayApp.pesawat.repo;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.pesawat.entity.Penumpang;

public interface PenumpangRepo extends JpaRepository<Penumpang, Long> {

//	Page<Penumpang> search(String search, PageRequest paging);


}
