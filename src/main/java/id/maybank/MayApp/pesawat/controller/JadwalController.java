package id.maybank.MayApp.pesawat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penerbangan;
import id.maybank.MayApp.pesawat.service.JadwalService;
import id.maybank.MayApp.pesawat.service.PenerbanganService;

@Controller
@RequestMapping("/add-tiket-pesawat")
public class JadwalController {
	
	@Autowired
	private JadwalService jadwalService;
	
	@Autowired
	private PenerbanganService penerbanganService;
	
	@GetMapping
	public String index(
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo,
            @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
            @RequestParam(value="sortField", defaultValue = "id") String sortField,
            @RequestParam (value = "search", defaultValue = "") String search,
            Model model) {
		
		Page<Jadwal> jadwals;
		

		if (search != null) {
			jadwals = this.jadwalService.getAllPaginateSearch(pageNo, pageSize, sortField, search);

		}else {
			jadwals = this.jadwalService.getAllPaginate(pageNo, pageSize, sortField);

				
		}
		
		model.addAttribute("page", jadwals);
		model.addAttribute("jadwalForm", new Jadwal());
		model.addAttribute("search", search);
		return "jadwal";

	}
	
	
	@PostMapping("/save")
    public String save(@ModelAttribute("jadwalForm")
            Jadwal jadwal, 
            BindingResult result, //add binding result
            RedirectAttributes redirectAttributes,
            @RequestParam(value="page", defaultValue = "0") int pageNo,
            @RequestParam(value="Size", defaultValue = "5") int pageSize,
            @RequestParam(value="sortField", defaultValue = "id") String sortField,
            @RequestParam (value = "search", defaultValue = "") String search,
            Model model) {

            this.jadwalService.save(jadwal);
            redirectAttributes.addFlashAttribute("success","data inserted");
            return "redirect:/add-tiket-pesawat";
            
	}
	
	
	@GetMapping("/delete")
	public String delete(Jadwal jadwal, RedirectAttributes redirectAttributes) {
		this.jadwalService.delete(jadwal.getId());
		
		return "redirect:/add-tiket-pesawat";
	}
	
	
	@GetMapping("/edit")
	public String edit(@RequestParam("id")Long id, Model model) {
		Optional<Jadwal> jadwals = this.jadwalService.getJadwalById(id);
		model.addAttribute("jadwalForm", jadwals);
		return "edit-jadwal";
	}
	
	@GetMapping("/edit-pesanan")
	public String editPesanan(@RequestParam("id")Long id, Model model) {
		Optional<Penerbangan> penerbangans = this.penerbanganService.getPenerbanganById(id);
		model.addAttribute("penerbanganForm", penerbangans);
		return "edit-riwayat-pesanan";
	}
	
	@PostMapping("/save-pesanan")
    public String savePesanan(@ModelAttribute("penerbanganForm")
            Penerbangan penerbangans, 
            BindingResult result, //add binding result
            RedirectAttributes redirectAttributes,
            Model model) {
		
			penerbangans.setStatus("Lunas");
            this.penerbanganService.save(penerbangans);
            redirectAttributes.addFlashAttribute("success","data inserted");
            return "edit-riwayat-pesanan";
            
	}
	
	@GetMapping("/riwayat-pesanan")
	public String indexHTML(
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo,
            @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
            @RequestParam(value="sortField", defaultValue = "id") String sortField,
            @RequestParam (value = "search", defaultValue = "") String search,
            Model model) {
		
		
		List<Penerbangan> penerbangans = this.penerbanganService.getAll();
		
		model.addAttribute("penerbangans", penerbangans);
		return "admin-riwayat-pesanan";

	}
	
	
	
	
        

}
