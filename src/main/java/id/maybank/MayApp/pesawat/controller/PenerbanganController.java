package id.maybank.MayApp.pesawat.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.MayApp.pesawat.entity.Jadwal;
import id.maybank.MayApp.pesawat.entity.Penerbangan;
import id.maybank.MayApp.pesawat.entity.Penumpang;
import id.maybank.MayApp.pesawat.service.JadwalService;
import id.maybank.MayApp.pesawat.service.PenerbanganService;
import id.maybank.MayApp.pesawat.service.PenumpangService;

@Controller
@RequestMapping("/tiket-pesawat")
public class PenerbanganController {
	
	@Autowired
	private PenerbanganService penerbanganService;
	
	@Autowired
	private JadwalService jadwalService;
	
	@Autowired
	private PenumpangService penumpangService;
	
	@GetMapping
	public String index(
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo,
            @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
            @RequestParam(value="sortField", defaultValue = "id") String sortField,
            @RequestParam (value = "search", defaultValue = "") String search,
            @RequestParam (value = "search2", defaultValue = "") String search2,

            Model model) {
		List<Jadwal> penerbangans;
//		List<Penumpang> penumpangs = this.penumpangService.getAll();
//		Penumpang penumpangs = penumpangService.getPenumpangById(id);
		if (search!= null) {
			penerbangans = this.jadwalService.getAllSearch(search,search2);

		}else {
			penerbangans = this.jadwalService.getAll();

		}

		model.addAttribute("jadwals", penerbangans);
	    model.addAttribute("penumpangForm", new Penumpang());
		model.addAttribute("penerbanganForm", new Penerbangan());

		model.addAttribute("search", search);

		return "penerbangan";
	}
	
	
	@PostMapping("/save")
    public String save(@ModelAttribute("penerbanganForm")
            Penerbangan penerbangan, 
            @ModelAttribute("penumpangForm")Penumpang penumpangs,
            @ModelAttribute("jadwalForm")Jadwal jadwal,
            BindingResult result, //add binding result
            RedirectAttributes redirectAttributes,
  
            Model model) {
		
//			System.out.println("babbbb "+jadwal.getSeats());
			Jadwal jadwalNew = this.jadwalService.getJadwalByFlight(jadwal.getFlight());
			
    		jadwalNew.setSeats(jadwal.getSeats()-penumpangs.getJmlKursi());
			

    		this.jadwalService.save(jadwalNew);
    		this.penumpangService.save(penumpangs);

			penerbangan.setStatus("Belum Bayar");
			penerbangan.setPenumpangs(penumpangs);
            this.penerbanganService.save(penerbangan);
            redirectAttributes.addFlashAttribute("success","data inserted");
            return "redirect:/tiket-pesawat/riwayat";
            
	}
	
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id")Long id, RedirectAttributes redirectAttributes) {
		this.penerbanganService.delete(id);
		return "redirect:/tiket-pesawat/riwayat";
	}
	
	
	@GetMapping("/pesan")
	public String edit(@RequestParam("id")Long id, Model model) {
		Optional<Jadwal> jadwals = this.jadwalService.getJadwalById(id);
	    model.addAttribute("penumpangForm", new Penumpang());
		model.addAttribute("penerbanganForm", jadwals);

		model.addAttribute("jadwalForm", jadwals);
		return "edit-penerbangan";
	}
	
	@GetMapping("/riwayat")
	public String riwayat(
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo,
            @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
            @RequestParam(value="sortField", defaultValue = "id") String sortField,
            Model model) {
		Page<Penerbangan> penerbangans = this.penerbanganService.getAllPaginates(pageNo, pageSize, sortField);
		model.addAttribute("penerbangans", penerbangans);
		return "riwayat-pesanan";
	}
	
	
	
//	@GetMapping("/search")
//	public String search(
//			@RequestParam (value = "search1", defaultValue = "") String search1,
//            @RequestParam (value = "search1", defaultValue = "") String search2,
//            Model model) {
//		Page<Jadwal> penerbangans;
//		if (search1!= null && search2 != null) {
//			penerbangans = this.penerbanganService.getAllPaginateSearch(pageNo, pageSize, sortField, search1, search2);
//
//		}else {
//			penerbangans = this.penerbanganService.getAllPaginate(pageNo, pageSize, sortField);
//
//				
//		}
//		Optional<Penerbangan> penerbangans = this.penerbanganService.getPenerbanganById(id);
//		model.addAttribute("penerbanganForm", penerbangans);
//		return "edit-penerbangan";
//	}
	
        

}
