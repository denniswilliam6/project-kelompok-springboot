package id.maybank.MayApp.pesawat.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="penumpang")
public class Penumpang {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nama;
	private String noId;
	private String title;
	private String tanggal;
	private String jam;
	private int jmlKursi;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "penumpangs")
	private List<Penerbangan> penerbangans;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public int getJmlKursi() {
		return jmlKursi;
	}
	public void setJmlKursi(int jmlKursi) {
		this.jmlKursi = jmlKursi;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoId() {
		return noId;
	}
	public void setNoId(String noId) {
		this.noId = noId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Penerbangan> getPenerbangan() {
		return penerbangans;
	}
	public void setPenerbangan(List<Penerbangan> penerbangans) {
		this.penerbangans = penerbangans;
	}
	
	

	
}
