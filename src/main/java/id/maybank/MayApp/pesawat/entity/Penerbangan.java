package id.maybank.MayApp.pesawat.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="penerbangan")
public class Penerbangan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String flight;
	private String source;
	private String destination;
	private String tanggal;
	private String jam;
	private int jmlKursi;
	private String price;
	private String status;
	
	@ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "jadwal_id", referencedColumnName = "id")
	private Jadwal jadwal;
	
	
	@ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "penumpangs_id", referencedColumnName = "id")
	private Penumpang penumpangs;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public int getJmlKursi() {
		return jmlKursi;
	}
	public void setJmlKursi(int jmlKursi) {
		this.jmlKursi = jmlKursi;
	}
	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Jadwal getJadwal() {
		return jadwal;
	}
	public void setJadwal(Jadwal jadwal) {
		this.jadwal = jadwal;
	}
	public Penumpang getPenumpangs() {
		return penumpangs;
	}
	public void setPenumpangs(Penumpang penumpangs) {
		this.penumpangs = penumpangs;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	

	
}
