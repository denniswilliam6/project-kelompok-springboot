package id.maybank.MayApp.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "provider")
public class Provider {
	
		@Id
		@GeneratedValue( strategy = GenerationType.AUTO)
		private Long id;
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "provider", cascade = CascadeType.ALL)
		private List<PaketData> paketdata;
		private String namaprovider;
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public List<PaketData> getPaketdata() {
			return paketdata;
		}
		public void setPaketdata(List<PaketData> paketdata) {
			this.paketdata = paketdata;
		}
		public String getNamaprovider() {
			return namaprovider;
		}
		public void setNamaprovider(String namaprovider) {
			this.namaprovider = namaprovider;
		}
		


}
