package id.maybank.MayApp.controller;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.MayApp.entity.Provider;
import id.maybank.MayApp.service.ProviderService;



@Controller
@RequestMapping("/provider")
public class ProviderController {

	@Autowired
	private ProviderService providerService;
	
	@GetMapping
    public String index(Model model) {
        List<Provider> providers = this.providerService.getAll();
        model.addAttribute("providers", providers);
        model.addAttribute("providerForm", new Provider());
        return "provider";
    }
	
	
	@PostMapping("/save")
	public String save(Provider provider, RedirectAttributes redirectAttributes) {
			this.providerService.save(provider);
			redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/provider";
		}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id")Long id) {
		Optional<Provider> provider = this.providerService.getProviderById(id);
		if(provider.isPresent()){
			this.providerService.delete(provider.get());
		}
		
		return"redirect:/provider";
	}

}
