package id.maybank.MayApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import id.maybank.MayApp.entity.PaketData;
import id.maybank.MayApp.entity.Provider;
import id.maybank.MayApp.service.PaketDataService;
import id.maybank.MayApp.service.ProviderService;


@Controller
@RequestMapping("/paketdata")		
public class PaketDataController {

	
	@Autowired
	private PaketDataService paketdataService;
	
	@Autowired
	private ProviderService providerService;
	
	@GetMapping
	public String index(Model model) { 
		List<PaketData> paketdatas = this.paketdataService.getAll(); 
		List<Provider> providers = this.providerService.getAll(); 
		
		model.addAttribute("paketdatas", paketdatas);
		model.addAttribute("providers", providers);
		

		model.addAttribute("paketdataForm",new PaketData());
		model.addAttribute("providerForm",new Provider());
		return "paketdata";
	}
	
	@PostMapping("/save")
	public String save(
			@ModelAttribute("providerForm")Provider provider,
			@ModelAttribute("paketdataForm")PaketData paketdata, Model model){
		paketdata.setProvider(provider);
		this.paketdataService.save(paketdata);
		
		return "redirect:/paketdata";
		}
	}