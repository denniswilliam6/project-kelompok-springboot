package id.maybank.MayApp.controller;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.MayApp.service.BeliPaketService;
import id.maybank.MayApp.service.PaketDataService;
import id.maybank.MayApp.entity.BeliPaket;
import id.maybank.MayApp.entity.PaketData;
import id.maybank.MayApp.entity.Provider;


@Controller
@RequestMapping("/belipaket")
public class BeliPaketController {

	@Autowired
	private BeliPaketService belipaketService;
	
	@Autowired
	private PaketDataService paketdataService;
	
	@GetMapping
    public String index(Model model) {
        List<BeliPaket> belis = this.belipaketService.getAll();
        model.addAttribute("belis", belis);
        List<PaketData> paketDatas= this.paketdataService.getAll();
        model.addAttribute("beliForm", new BeliPaket());
        model.addAttribute("paketDatas", paketDatas);
        return "belipaket";
    }
	
	
	@PostMapping("/save")
	public String save(
			@ModelAttribute("paketdataForm")PaketData paketdata,
			@ModelAttribute("beliForm")BeliPaket beliPaket,
			RedirectAttributes redirectAttributes){
			beliPaket.setPaketData(paketdata.getPaketData());
			beliPaket.setProvider(paketdata.getProvider().getNamaprovider());
			this.belipaketService.save(beliPaket);
			redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/belipaket";
		}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id")Long id) {
		Optional<BeliPaket> beliPaket = this.belipaketService.getBeliById(id);
		if(beliPaket.isPresent()){
			this.belipaketService.delete(beliPaket.get());
		}
		
		return"redirect:/belipaket";
	}

}
