package id.maybank.MayApp.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.Role;
import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.repo.RoleRepo;
import id.maybank.MayApp.ecommerce.repo.UserRepo;


@Service
@Transactional
public class InitDefaultUser {

	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	@PostConstruct
	public void index() {
		Role roleUser = new Role();
		Role roleAdmin = new Role();
		
		roleUser.setRole("user");
		roleAdmin.setRole("admin");
		
		this.roleRepo.save(roleUser);
		this.roleRepo.save(roleAdmin);
		
		List<Role> listUser = new ArrayList<>();
		List<Role> listAdmin = new ArrayList<>();
		
		listUser.add(roleUser);
		listAdmin.add(roleAdmin);
		
		User userAdmin = new User();
		userAdmin.setUsername("admin");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("12345678"));
		userAdmin.setRoles(listAdmin);
		
		
		User userUser = new User();
		userUser.setUsername("Bambang");
		userUser.setNama("Bambang S");
		userUser.setSaldo(10000.0);
		userUser.setPassword(new BCryptPasswordEncoder().encode("12345678"));
		userUser.setRoles(listUser);
		
		
		this.userRepo.save(userAdmin);
		this.userRepo.save(userUser);
	}
}
