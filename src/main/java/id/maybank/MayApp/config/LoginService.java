package id.maybank.MayApp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.repo.UserRepo;


@Service
public class LoginService {

	@Autowired
	private UserRepo userRepo;
	
	public User findUserByUsername(String username) {
		return this.userRepo.findByUsername(username);
	}
}
