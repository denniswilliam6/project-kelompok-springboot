package id.maybank.MayApp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
public class MyAuth {

	@Autowired
	private CostumDetailService detailService;
	@Autowired
	private UserAuthenticationHandler userAuthenticationHandler;
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(detailService);
		authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authenticationProvider;
	}
	
	@Bean BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public SecurityFilterChain securityFilterChain
				(HttpSecurity httpSecurity) throws Exception{
//		httpSecurity.authorizeRequests().antMatchers("/provider")
//			.hasAnyAuthority("admin");
//		httpSecurity.authorizeRequests().antMatchers("/operator")
//			.hasAnyAuthority("operator");
//		httpSecurity.authorizeRequests().antMatchers("/nasabah")
//		.hasAnyAuthority("customerservice");
		httpSecurity.csrf().disable();
			httpSecurity.authorizeRequests().anyRequest().authenticated();
		httpSecurity.authorizeRequests().and().formLogin().successHandler(userAuthenticationHandler);
		
		return httpSecurity.build();
	}
}
