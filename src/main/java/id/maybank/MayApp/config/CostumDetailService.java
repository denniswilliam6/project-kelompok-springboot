package id.maybank.MayApp.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.Role;
import id.maybank.MayApp.ecommerce.model.User;



@Service
public class CostumDetailService implements UserDetailsService {
	
	@Autowired
	private LoginService loginService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User userApp = this.loginService.findUserByUsername(username);
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		if(userApp != null) {
			List<Role> roles = userApp.getRoles();
			if(roles.size()>0) {
				for(Role role : roles) {
					authorities.add(new SimpleGrantedAuthority(role.getRole()));
				}
			}
			UserDetails userDetails = org.springframework.security.core.userdetails.User.withUsername(userApp.getUsername())
					.password(userApp.getPassword())
					.authorities(authorities)
					.build();
			return userDetails;
		}else {
			throw new UsernameNotFoundException("user not found");
		}
		
	}
}
