package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import id.maybank.MayApp.entity.BeliPaket;


public interface BeliPaketService {
	
	public List<BeliPaket> getAll();
	public void save(BeliPaket beliPaket);
	public void delete(BeliPaket beliPaket);
	
	public Optional<BeliPaket> getBeliById(Long id);

}
