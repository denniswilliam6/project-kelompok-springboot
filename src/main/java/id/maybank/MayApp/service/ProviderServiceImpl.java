package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.entity.Provider;
import id.maybank.MayApp.repo.ProviderRepo;

@Service
public class ProviderServiceImpl implements ProviderService {
	
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.save(provider);
	}

	@Override
	public void delete(Provider provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<Provider> getProviderById(Long id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

}
