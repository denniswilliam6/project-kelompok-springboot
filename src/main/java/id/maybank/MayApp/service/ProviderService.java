package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import id.maybank.MayApp.entity.Provider;

public interface ProviderService {
 	public List<Provider> getAll();
    public void save(Provider provider);
    public void delete(Provider provider);
    public Optional<Provider> getProviderById(Long id);
}
