package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.entity.BeliPaket;
import id.maybank.MayApp.repo.BeliPaketRepo;

@Service
public class BeliPaketServiceImpl implements BeliPaketService {

	@Autowired
	private BeliPaketRepo admpaketdataRepo;
	
	@Override
	public List<BeliPaket> getAll() {
		// TODO Auto-generated method stub
		return this.admpaketdataRepo.findAll();
	}

	@Override
	public void save(BeliPaket beliPaket) {
		// TODO Auto-generated method stub
		this.admpaketdataRepo.save(beliPaket);
	}

	@Override
	public void delete(BeliPaket beliPaket) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Optional<BeliPaket> getBeliById(Long id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

}
