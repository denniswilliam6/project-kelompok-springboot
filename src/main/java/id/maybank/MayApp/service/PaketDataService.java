package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import id.maybank.MayApp.entity.PaketData;


public interface PaketDataService {
 	public List<PaketData> getAll();
    public void save(PaketData paketdata);
    public void delete(PaketData paketdata);
    public Optional<PaketData> getPaketDataById(Long id);
}
