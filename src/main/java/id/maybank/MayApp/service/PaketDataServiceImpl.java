package id.maybank.MayApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import id.maybank.MayApp.entity.PaketData;
import id.maybank.MayApp.repo.PaketDataRepo;

@Service
public class PaketDataServiceImpl implements PaketDataService {
	
	@Autowired
	private PaketDataRepo paketdataRepo;

	@Override
	public List<PaketData> getAll() {
		// TODO Auto-generated method stub
		return this.paketdataRepo.findAll();
	}

	@Override
	public void save(PaketData paketdata) {
		// TODO Auto-generated method stub
		this.paketdataRepo.save(paketdata);
	}

	@Override
	public void delete(PaketData paketdata) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Optional<PaketData> getPaketDataById(Long id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

}
