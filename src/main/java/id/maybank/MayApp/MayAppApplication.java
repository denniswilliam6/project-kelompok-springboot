package id.maybank.MayApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.service.BarangService;
import id.maybank.MayApp.ecommerce.service.UserService;

@SpringBootApplication
public class MayAppApplication implements ApplicationRunner{
	@Autowired
	private UserService userService;
	
	@Autowired
	private BarangService barangService;
	
	public static void main(String[] args) {
		SpringApplication.run(MayAppApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
	
		Barang barang = new Barang();
		Barang barang2 = new Barang();
		barang.setNamaBarang("Samsung S20");
		barang2.setNamaBarang("MacBook Pro");
		barang.setHarga(1000.0);
		barang2.setHarga(2000.0);
		barang.setImage("samsungS20.jpg");
		barang2.setImage("macbookpro.jpeg");
		barang.setDeskripsi("Samsung S20 Blue 128 Gb");
		barang2.setDeskripsi("Macbook Pro Silver 128 Gb");
		this.barangService.saveBarang(barang);
		this.barangService.saveBarang(barang2);
	}

}
