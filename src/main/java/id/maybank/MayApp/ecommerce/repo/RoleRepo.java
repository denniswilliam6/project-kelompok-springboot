package id.maybank.MayApp.ecommerce.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.ecommerce.model.Role;


public interface RoleRepo extends JpaRepository<Role, Long>{

}
