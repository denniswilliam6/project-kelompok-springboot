package id.maybank.MayApp.ecommerce.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.ecommerce.model.User;

public interface UserRepo extends JpaRepository<User, Long>{

	User findUserByNama(String nama);
	User findUserById(Long id);
	User findByUsername(String username);
}
