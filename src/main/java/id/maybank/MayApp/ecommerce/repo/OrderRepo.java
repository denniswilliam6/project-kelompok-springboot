package id.maybank.MayApp.ecommerce.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.ecommerce.model.Order;

public interface OrderRepo extends JpaRepository<Order, Long >{

	Order findOrderByStatus(String status);
	
	Order findOrderById(Long id);
}
