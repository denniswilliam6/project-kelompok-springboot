package id.maybank.MayApp.ecommerce.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.ecommerce.model.Barang;

public interface BarangRepo extends JpaRepository<Barang, Long>{

	Barang findBarangById(Long id);
}
