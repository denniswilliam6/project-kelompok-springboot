package id.maybank.MayApp.ecommerce.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.MayApp.ecommerce.model.OrderDetail;

public interface OrderDetailRepo extends JpaRepository<OrderDetail, Long>{

}
