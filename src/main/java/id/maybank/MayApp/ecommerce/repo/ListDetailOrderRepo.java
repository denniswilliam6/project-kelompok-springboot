package id.maybank.MayApp.ecommerce.repo;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.maybank.MayApp.ecommerce.model.ListDetailOrder;

public interface ListDetailOrderRepo extends JpaRepository<ListDetailOrder, Long>{
	
}
