package id.maybank.MayApp.ecommerce.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.model.Order;
import id.maybank.MayApp.ecommerce.model.OrderDetail;
import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.service.BarangService;
import id.maybank.MayApp.ecommerce.service.OrderDetailService;
import id.maybank.MayApp.ecommerce.service.OrderService;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private BarangService barangService;

	@Autowired
	private OrderDetailService orderDetailService;

	private String image;

	@GetMapping
	public String index(Model model, @ModelAttribute("orderForm") OrderDetail order,
			@ModelAttribute("barangForm") Barang barang) {
		List<Barang> barangs = this.barangService.getAllBarang();
		model.addAttribute("barangs", barangs);
		return "order";
	}

	@GetMapping("/detail")
	public String detailOrder(@RequestParam("id") Long id, Model model, @ModelAttribute("orderForm") OrderDetail order,
			@ModelAttribute("barangForm") Barang barang) {
		Barang barang1 = this.barangService.getBarangById(id);
		image = barang1.getImage();
		model.addAttribute("barangForm", barang1);

		return "order_detail";
	}

	@PostMapping("/detail/save")
	public String saveOrder(@ModelAttribute("barangForm") Barang barang,
			@ModelAttribute("orderForm") OrderDetail order) {
		Integer jumlah = order.getJumlah();
		orderDetailService.saveOrder(barang, jumlah, image);
		return "redirect:/order";
	}

}
