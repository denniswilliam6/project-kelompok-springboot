package id.maybank.MayApp.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.MayApp.ecommerce.model.ListDetailOrder;
import id.maybank.MayApp.ecommerce.model.OrderDetail;
import id.maybank.MayApp.ecommerce.repo.ListDetailOrderRepo;
import id.maybank.MayApp.ecommerce.service.ListDetailOrderService;
import id.maybank.MayApp.ecommerce.service.OrderDetailService;
import id.maybank.MayApp.ecommerce.service.OrderService;

@Controller
@RequestMapping("/cart")
public class CartController {

	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired
	private OrderService orderService;
	
	@GetMapping
	public String index(Model model) {
		List<OrderDetail> ordes = this.orderDetailService.getAllOrderDetail();
		model.addAttribute("ordes",ordes);
		return "cart";
	}
	
	@GetMapping("/delete")
	public String deleBarang(@RequestParam("id") Long id) {
		this.orderDetailService.deleteOrderDetail(id);
		return "redirect:/cart";
	}
	
	@PostMapping("/save")
	public String save(RedirectAttributes redirectAttributes) throws Exception {
		this.orderService.save();
		this.orderDetailService.deleteAllOrderDetail();
		
		return "redirect:/payment";
		
	}
}
