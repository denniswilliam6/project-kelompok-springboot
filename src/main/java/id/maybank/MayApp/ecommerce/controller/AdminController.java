package id.maybank.MayApp.ecommerce.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.model.Order;
import id.maybank.MayApp.ecommerce.service.BarangService;
import id.maybank.MayApp.ecommerce.service.OrderService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private BarangService barangService;
	@Autowired
	private OrderService orderService;
	
	@GetMapping
	public String index(Model model) {
		List<Barang> barangs = this.barangService.getAllBarang();
		model.addAttribute("barangs", barangs);
		model.addAttribute("barangForm", new Barang());
		return "tambah_barang";
	}
	
	@PostMapping("/save")
	public String tambahBarang(@Valid @ModelAttribute("barangForm") Barang barang,
			BindingResult result,
			@RequestParam(name = "file")MultipartFile image,
			
			Model model) {
		if(result.hasErrors()) {
			List<Barang> barangs = this.barangService.getAllBarang();
			model.addAttribute("barangs", barangs);
			return "tambah_barang";
		}
		 if(!image.isEmpty()) {
		    	Path directori= Paths.get("src//main//resources//static/images");
		        String route = directori.toFile().getAbsolutePath();
		        
		        try {
					byte[] bytesImg = image.getBytes();
					Path rutaCompleta = Paths.get(route + "//" + image.getOriginalFilename());
					Files.write(rutaCompleta, bytesImg);
					barang.setImage(image.getOriginalFilename());
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		this.barangService.saveBarang(barang);
		return "redirect:/admin";
	}
	
	@GetMapping("/delete")
	public String deleBarang(@RequestParam("id") Long id) {
		this.barangService.deleteBarang(id);
		return "redirect:/admin";
	}
	
	@GetMapping("/edit")
	public String editBarang(@RequestParam("id")Long id, Model model) {
		Barang barang = this.barangService.getBarangById(id);
		this.barangService.deleteBarang(id);
		model.addAttribute("barangForm",barang);
		return "edit_barang";
	}
	
	@GetMapping("/invoice")
	public String invoice(Model model) {
		List<Order> orders = this.orderService.getAllOrder();
		model.addAttribute("orders",orders);
		return "invoice";
	}
	
	@RequestMapping("/invoice/getOne")
	@ResponseBody
	public Optional<Order> getOne(Long id){
		return orderService.getOne(id);
	}
	
	@RequestMapping(value="invoice/update", method = {RequestMethod.PUT, RequestMethod.GET})
	public String update(Order order) {
		this.orderService.update(order);
		return "redirect:/admin/invoice";		
	}

}
