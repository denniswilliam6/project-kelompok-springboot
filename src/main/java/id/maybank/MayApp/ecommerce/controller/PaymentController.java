package id.maybank.MayApp.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import id.maybank.MayApp.ecommerce.model.Order;
import id.maybank.MayApp.ecommerce.service.OrderService;

@Controller
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	private OrderService orderService;
	
	@GetMapping
	String index(Model model) {
		List<Order> orders = this.orderService.getAllOrder();
		model.addAttribute("orders", orders);
		return "payment";
	}
}
