package id.maybank.MayApp.ecommerce.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.repo.UserRepo;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepo userRepo;
	
	public void saveUser(User user) {
		this.userRepo.save(user);
	}
}



