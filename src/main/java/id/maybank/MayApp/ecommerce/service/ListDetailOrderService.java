package id.maybank.MayApp.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.ListDetailOrder;
import id.maybank.MayApp.ecommerce.model.OrderDetail;
import id.maybank.MayApp.ecommerce.repo.ListDetailOrderRepo;
import id.maybank.MayApp.ecommerce.repo.OrderDetailRepo;

@Service
public class ListDetailOrderService {
	
	@Autowired
	private ListDetailOrderRepo listDetailOrderRepo;
	@Autowired
	private OrderDetailRepo orderDetailRepo;

	public List<ListDetailOrder> findAllDetailOrders(){
		return this.listDetailOrderRepo.findAll();
	}
	
	public void saveListDetailOrder () {
		List<OrderDetail>listDetail = this.orderDetailRepo.findAll();
		Double sumDouble = 0.0;
		for(OrderDetail dataDetail : listDetail) {
			ListDetailOrder newData = new ListDetailOrder();
			
			newData.setNamaBarang(dataDetail.getNamaBarang());
			newData.setJumlah(dataDetail.getJumlah());
			newData.setDeskripsi(dataDetail.getDeskripsi());
			newData.setHarga(dataDetail.getHarga());
			newData.setOrder(null);
			
			Double jumlah = new Double(dataDetail.getJumlah());
			Double totalDouble = dataDetail.getHarga() * jumlah;
			sumDouble = Double.sum(sumDouble, totalDouble);
			
			newData.setTotal(totalDouble);
			this.listDetailOrderRepo.save(newData);
			this.listDetailOrderRepo.flush();
		}
	}
}
