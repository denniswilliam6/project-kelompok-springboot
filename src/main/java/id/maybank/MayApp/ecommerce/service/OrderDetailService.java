package id.maybank.MayApp.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.model.OrderDetail;
import id.maybank.MayApp.ecommerce.repo.OrderDetailRepo;

@Service
public class OrderDetailService {
	
	@Autowired
	private OrderDetailRepo orderDetailRepo;

	public void saveOrder(Barang barang,Integer jumlah,String image) {
		
		OrderDetail detail = new OrderDetail();
		detail.setNamaBarang(barang.getNamaBarang());
		detail.setHarga(barang.getHarga());
		detail.setDeskripsi(barang.getDeskripsi());
		detail.setJumlah(jumlah);
		detail.setImage(image);
		
		this.orderDetailRepo.save(detail);
	
	}
	
	
	public List<OrderDetail> getAllOrderDetail() {
		return this.orderDetailRepo.findAll();
	}
	
	public void deleteOrderDetail(Long id) {
		this.orderDetailRepo.deleteById(id);
	}
	
	public void deleteAllOrderDetail() {
		this.orderDetailRepo.deleteAll();
	}
}
