package id.maybank.MayApp.ecommerce.service;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

//import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.model.ListDetailOrder;
import id.maybank.MayApp.ecommerce.model.Order;
import id.maybank.MayApp.ecommerce.model.OrderDetail;
import id.maybank.MayApp.ecommerce.model.User;
import id.maybank.MayApp.ecommerce.repo.ListDetailOrderRepo;
import id.maybank.MayApp.ecommerce.repo.OrderDetailRepo;
import id.maybank.MayApp.ecommerce.repo.OrderRepo;
import id.maybank.MayApp.ecommerce.repo.UserRepo;

@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderRepo orderRepo;

	@Autowired
	private OrderDetailRepo orderDetailRepo;

	@Autowired
	private ListDetailOrderRepo listDetailOrderRepo;

	@Autowired
	private UserRepo userRepo;

	public List<Order> getAllOrder() {
		return this.orderRepo.findAll();
	}

	@Transactional
	public void save() throws Exception {
		try {
			User user = this.userRepo.findUserByNama("Bambang S");
			Order order = new Order();
			order.setStatus("proses");
			order.setUser(user);
			Date sqlDate = Date.valueOf(LocalDate.now());
			order.setTanggalPembelian(sqlDate);

			this.orderRepo.save(order);

			// Save Detail
			List<OrderDetail> listDetail = this.orderDetailRepo.findAll();
			Double sumDouble = 0.0;
			for (OrderDetail dataDetail : listDetail) {
				ListDetailOrder newData = new ListDetailOrder();

				newData.setNamaBarang(dataDetail.getNamaBarang());
				newData.setJumlah(dataDetail.getJumlah());
				newData.setDeskripsi(dataDetail.getDeskripsi());
				newData.setOrder(order);
				newData.setHarga(dataDetail.getHarga());

				Double jumlah = new Double(dataDetail.getJumlah());
				Double totalDouble = dataDetail.getHarga() * jumlah;
				sumDouble = Double.sum(sumDouble, totalDouble);

				newData.setTotal(totalDouble);
				this.listDetailOrderRepo.save(newData);
//				this.listDetailOrderRepo.flush();
			}

			Order newOrder = this.orderRepo.findOrderById(order.getId());
			newOrder.setTotalBayar(sumDouble);
			User newUser = this.userRepo.findUserById(order.getUser().getId());
			newUser.setSaldo(newUser.getSaldo() - sumDouble);

			Integer er = Double.compare(newUser.getSaldo(), 0);
			System.out.println(er);
			if (er < 0) {
				throw new RuntimeException("Saldo tidak Cukup!!");
			}

			this.userRepo.save(newUser);
			this.orderRepo.save(newOrder);

		} catch (RuntimeException e) {
			// rollback transaction
			throw new RuntimeException(e.getMessage());
			
		} catch (Exception e) {
			// commit transaction
		}

	}

	public Optional<Order> getOne(Long id) {
		return this.orderRepo.findById(id);
	}

	public void update(Order order) {
		this.orderRepo.save(order);
	}

}
