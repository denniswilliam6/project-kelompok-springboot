package id.maybank.MayApp.ecommerce.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.MayApp.ecommerce.model.Barang;
import id.maybank.MayApp.ecommerce.repo.BarangRepo;

@Service
@Transactional
public class BarangService {
	
	@Autowired
	BarangRepo barangRepo;

	public List<Barang> getAllBarang(){
		return this.barangRepo.findAll();
	}
	
	public void saveBarang(Barang barang) {
		this.barangRepo.save(barang);
	}
	
	public void deleteBarang(Long id) {
		this.barangRepo.deleteById(id);
	}
	
	public Barang getBarangById(Long id){
		return this.barangRepo.findBarangById(id);
	}
}
